
# A very simple Flask Hello World app for you to get started with...
import re
from jinja2 import evalcontextfilter, Markup, escape

from flask import Flask, redirect, render_template, request, url_for
from flask_sqlalchemy import SQLAlchemy

_paragraph_re = re.compile(r'(?:\r\n|\r|\n){2,}')


UPLOAD_FOLDER = 'static/uploads/'


app = Flask(__name__)
app.config["DEBUG"] = True
SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="nattaponwongs",
    password="Pp150343",
    hostname="nattaponwongs.mysql.pythonanywhere-services.com",
    databasename="nattaponwongs$Menu",
)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['MAX_CONTENT_LENGTH'] = 1024 * 1024
app.config['UPLOAD_EXTENSIONS'] = ['.jpg', '.png', '.gif']
app.config['UPLOAD_PATH'] = 'uploads'


db = SQLAlchemy(app)

class MENU(db.Model):

    __tablename__ = "menus"

    id = db.Column(db.Integer, primary_key=True)
    Name = db.Column(db.String(100))
    equipment = db.Column(db.String(1000))
    ingredient = db.Column(db.String(1000))
    image_name = db.Column(db.String(30))
    image = db.Column(db.LargeBinary)
    cooking = db.Column(db.String(10000))
    note = db.Column(db.String(4096))



@app.route("/insert", methods=["POST"])
def insert():

    menu = MENU(Name=request.form["HeadName"],equipment=request.form["equipment"], ingredient=request.form["ingredient"], cooking=request.form["cooking"], note=request.form["note"]    )

    db.session.add(menu)
    db.session.commit()
    return redirect("/admin_index")


@app.route("/update", methods=["GET", "POST"])
def update():
    if request.method == "GET":
       return render_template("admin_add.html", menus=MENU.query.all())

    id_temp = request.form["ID"]
    newName = request.form["HeadName"]
    newequipment = request.form["equipment"]
    newingredient = request.form["ingredient"]
    newcooking = request.form["cooking"]
    newnote = request.form["note"]
    menu = MENU.query.filter_by(id=id_temp).first()
    menu.Name = newName
    menu.equipment = newequipment
    menu.ingredient = newingredient
    menu.cooking = newcooking
    menu.note = newnote

    db.session.commit()
    return redirect("/admin_index")


@app.route("/delete/<id_temp>")
def delete(id_temp):
    menus = MENU.query.filter_by(id=id_temp).first()
    db.session.delete(menus)
    db.session.commit()
    return redirect("/admin_index")


@app.route("/")
def index():
    return render_template("main_page.html", menus=MENU.query.all())

@app.route("/admin_index")
def admin_index():
    return render_template("admin_add.html", menus=MENU.query.all())

@app.route("/admin")
def admin():
    return render_template("index_admin.html")


@app.route("/add", methods=["GET", "POST"])
def add():
    return render_template("Menu_add.html")


@app.route("/admin_mainpage", methods=["GET", "POST"])
def admin_mainpage():
    username = request.form["user-username"]
    password = request.form["user-password"]

    if username == 'test' and password == 'test':
        return render_template("admin_add.html", menus=MENU.query.all())
    else :
        return render_template("index_admin.html")

@app.route("/detail/<id_temp>")
def edit(id_temp):
    menus = MENU.query.filter_by(id=id_temp).one()
    return render_template("main_page_detail.html",id = menus.id,Name = menus.Name, equipment= menus.equipment,ingredient = menus.ingredient,cooking = menus.cooking,note = menus.note)


@app.route("/edit/<id_temp>")
def detail(id_temp):
    menus = MENU.query.filter_by(id=id_temp).one()
    return render_template("Menu_Edit.html",id = menus.id,Name = menus.Name, equipment= menus.equipment,ingredient = menus.ingredient,cooking = menus.cooking,note = menus.note)



@app.template_filter()
@evalcontextfilter
def nl2br(eval_ctx, value):
       result = u'\n\n'.join(u'<p>%s</p>' % p.replace('\n', '<br>\n') \
           for p in _paragraph_re.split(escape(value)))
       if eval_ctx.autoescape:
           result = Markup(result)
       return result




